﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;


namespace Domawka2__maski
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите путь к каталогу: ");
            string Path = Console.ReadLine();
            Console.Write("Введите что заменять(маску) для файлов: ");
            string Mask = Console.ReadLine();
            Console.Write("Введите текст для поиска в файлах: ");
            string Text = Console.ReadLine();

            // Дописываем слэш (в случае его отсутствия)
            if (Path[Path.Length - 1] != '\\')
                Path += '\\';

            string[] di = Directory.GetFiles(Path, "*.txt", SearchOption.AllDirectories);
            // Если путь не существует

            foreach (string fi in di)
            {
                //поиск и замена текста
                StreamReader input = new StreamReader(fi);
                string potok, stroka = "";

                while ((potok = input.ReadLine()) != null)
                {
                    stroka += potok + "\n";
                }
                input.Close();
                //поместил в строку мой текст
                Regex regex = new Regex(Mask);
                Match match = regex.Match(stroka);
                while (match.Success == true)
                {
                    stroka = regex.Replace(stroka, Text);
                    match = match.NextMatch();
                }
                Console.WriteLine(Path + @"\" + fi + " -ok!");
                StreamWriter output = new StreamWriter(fi);
                output.WriteLine(stroka);
                output.Close();
            }


            Console.WriteLine("ok!");
            Console.ReadLine();

        }

    }
}
